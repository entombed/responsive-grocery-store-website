const searchFormElement = document.querySelector('.search-form')
const shoppingCartElement = document.querySelector('.shopping-cart')
const loginFormElement = document.querySelector('.login-form')
const navbarElement = document.querySelector('.navbar')

document.querySelector('#search-btn').onclick = () => {
    searchFormElement.classList.toggle('active');
    shoppingCartElement.classList.remove('active');
    loginFormElement.classList.remove('active');
    navbarElement.classList.remove('active');
}

document.querySelector('#card-btn').onclick = () => {
    shoppingCartElement.classList.toggle('active')
    searchFormElement.classList.remove('active');
    loginFormElement.classList.remove('active');
    navbarElement.classList.remove('active');
}

document.querySelector('#login-btn').onclick = () => {
    loginFormElement.classList.toggle('active');
    searchFormElement.classList.remove('active');
    shoppingCartElement.classList.remove('active');
    navbarElement.classList.remove('active');
}

document.querySelector('#menu-btn').onclick = () => {
    navbarElement.classList.toggle('active')
    searchFormElement.classList.remove('active');
    shoppingCartElement.classList.remove('active');
    loginFormElement.classList.remove('active');
}

window.onscroll = () => {
    searchFormElement.classList.remove('active');
    shoppingCartElement.classList.remove('active');
    loginFormElement.classList.remove('active');
    navbarElement.classList.remove('active');
}

var swiper = new Swiper(".product-slider", {
    loop:true,
    spaceBetween: 20,
    autoplay: {
        delay: 7500,
        disableOnInteraction: false,
    },
    centeredSlides: true,
    breakpoints: {
        0: {
            slidesPerView: 1,
        },
        768: {
            slidesPerView: 2,
        },
        1020: {
            slidesPerView: 3,
        },
    },
});

var swiper = new Swiper(".review-slider", {
    loop:true,
    spaceBetween: 20,
    autoplay: {
        delay: 7500,
        disableOnInteraction: false,
    },
    centeredSlides: true,
    breakpoints: {
        0: {
            slidesPerView: 1,
        },
        768: {
            slidesPerView: 2,
        },
        1020: {
            slidesPerView: 3,
        },
    },
});

